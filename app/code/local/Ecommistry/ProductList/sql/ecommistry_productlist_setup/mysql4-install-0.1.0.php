<?php
$installer = $this;

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();

$data = array (
    'attribute_set' => 'Default',
    'label'         => 'Handle Display',
	'group'        	=> 'General',
	'input'         => 'select',
	'type'			=> 'int',
	'source'		=> 'eav/entity_attribute_source_boolean',
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'visible'       => true,
    'system'        => false,
    'required'      => true,
    'user_defined'  => true, 
);

$setup->removeAttribute('catalog_product','handle_display');
$setup->addAttribute('catalog_product','handle_display',$data);

$installer->endSetup();

        