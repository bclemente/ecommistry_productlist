<?php
class Ecommistry_ProductList_Block_Product_List extends Mage_Catalog_Block_Product_Abstract
{

    protected function _construct()
    {
        parent::_construct();

        $mode = $this->getRequest()->getParam('mode');

        if($mode == 'slideshow'){
            $this->setTemplate('ecommistry/customer/product/list.slideshow.phtml');
        } else {
            $this->setTemplate('ecommistry/customer/product/list.phtml');
        }
    }

    protected function _getProductCollection()
    {
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

        $limit = Mage::getStoreConfig('ecommistry/configuration/limit');

        $collection->addAttributeToSelect(array('name','small_image','price'))
            ->addStoreFilter()
            ->addAttributeToFilter('handle_display', array('eq' => 1))
            ->setCurPage(1);


        if(isset($limit) && $limit > 0) {
            $collection->setPageSize($limit);
        }

        return $collection;
    }

    protected function _beforeToHtml()
    {
        $this->setProductCollection($this->_getProductCollection());
        return parent::_beforeToHtml();
    }

}
