<?php
class Ecommistry_ProductList_ProductController extends Mage_Core_Controller_Front_Action {

    protected function _getSession() {
        return Mage::getSingleton('customer/session');
    }

    public function preDispatch() {
        parent::preDispatch();
        if(Mage::getStoreConfig('ecommistry/configuration/status')){
            if (!Mage::getSingleton('customer/session')->authenticate($this)) {
                    $this->setFlag('', 'no-dispatch', true);
            }
        } else {
            $this->norouteAction();
            return;
        }

    }

    public function listAction() {
        $this->loadLayout();
        $this->renderLayout();
    }
    
}