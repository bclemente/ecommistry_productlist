# README #

Ecommistry_ProductList extension

### What is this repository for? ###

* The extension will list all products with "handle_display" attribute set to "Yes" in the My Account section of the customer

### How do I get set up? ###

* Install the "app" and "skin" directory into Magento root directory from the repo
* Once the extension has been installed, Enable the extension by navigating to Admin > System > Configuration > Ecommistry > Ecommistry - Product List
![Screen Shot 2017-03-31 at 10.53.13 AM.png](https://bitbucket.org/repo/akbajya/images/527940609-Screen%20Shot%202017-03-31%20at%2010.53.13%20AM.png)

* This will enable "Product List" link in the Customer Navigation on My Account section
![Screen Shot 2017-03-31 at 11.05.24 AM.png](https://bitbucket.org/repo/akbajya/images/88827284-Screen%20Shot%202017-03-31%20at%2011.05.24%20AM.png)

* This will also create an attribute "Handle Display" and is assigned to all products
![Screen Shot 2017-03-31 at 11.00.59 AM.png](https://bitbucket.org/repo/akbajya/images/4140397876-Screen%20Shot%202017-03-31%20at%2011.00.59%20AM.png)

* Updating a product with "Handle Display" attribute set to "Yes", will display the product under "Product List" of My Account section. And product list can be viewed in Grid view or Slideshow
![Screen Shot 2017-03-31 at 11.18.55 AM.png](https://bitbucket.org/repo/akbajya/images/3777313526-Screen%20Shot%202017-03-31%20at%2011.18.55%20AM.png)
![Screen Shot 2017-03-31 at 11.19.35 AM.png](https://bitbucket.org/repo/akbajya/images/562614611-Screen%20Shot%202017-03-31%20at%2011.19.35%20AM.png)